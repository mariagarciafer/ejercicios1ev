package unidad3;

import java.util.Scanner;

public class Triángulo {

	public static void main(String[] args) {

//		En el método main de una clase llamada Triángulo escribe un programa que
//		determine si tres números reales que se introducen por teclado pueden ser las
//		longitudes de los lados de un triángulo. Si la respuesta es afirmativa, tendrá que
//		responder a las preguntas siguientes:
//		• ¿Es escaleno?
//		• ¿Es equilátero?
//		• ¿Es isósceles?
// 
//		Primero pregunto por teclado tres numeros reales y los asocio a tres variables x,y,z: 

		Scanner in = new Scanner(System.in);
		System.out.println("Introduce el primer número: ");
		int x = in.nextInt();

		System.out.println("Introduce el segundo número: ");
		int y = in.nextInt();

		System.out.println("Introduce el tercer número: ");
		int z = in.nextInt();

//		Pueden ser longitudes de los lados de un tríangulo si la suma de dos cualesquiera de ellos es mayor que el otro, por ejemplo x+y >z 

		if (x == y && y == z)
			System.out.println("El triángulo es equilátero");

		else if (x >= (y + z) || z >= (y + x) || y >= (x + z))
			System.out.println("No es un triángulo");

		else if ((x == y && y != z) || (x != y && z == x) || (z == y && z != x))
			System.out.println("El triángulo es isosceles");

		else if (x != y && y != z && z != x)
			System.out.println("El triángulo es escaleno");
	}
}

//		if ( ( (x+y) >z)) {
//			
//			if (x==y ) {
//				if (x==z) {
//					System.out.println("el tríangulo es equilatero");
//				}
//				if (x!=z) {
//					System.out.println("el tríangulo es isósceles");
//				
//			if	(x!=y & y!=z) {
//				System.out.println("el tríangulo es escaleno");
//			}
//			}
//				}
//				
//		}
//			}
//}
//
//
//
