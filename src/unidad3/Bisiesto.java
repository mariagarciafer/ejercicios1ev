package unidad3;

import java.util.Scanner;

public class Bisiesto {

	public static void main(String[] args) {
		
		
//		. En el método main de una clase llamada Bisiesto escribe un programa que
//		utilizando una única expresión lógica determine si un año es o no es bisiesto. El
//		año se introducirá por teclado y el resultado lo mostrará en la pantalla. Un año
//		es bisiesto si es múltiplo de 4. Los años múltiplos de 100 no son bisiestos, salvo
//		si son múltiplos de 400.
//	
	
		    Scanner in = new Scanner(System.in);
		 
		        // Necesito meter datos por teclado y pido el año
		    
		        System.out.println("Introduce un año");
		        int a = in.nextInt();
		 
		        //Si es divisible entre 4 y no es divisible entre 100 o es divisible entre 100 y 400.
		        
		        if ((a % 4 == 0 && a % 100 != 0) || (a % 100 == 0 && a % 400 == 0)) {
//		          &&  = evalúo la primera condicion y si es falsa ya no se evalúa la segunda 
//		        	!=0 el resto de dividir año entre 100 es distinto de 0 
//		        	|| si todo esto es verdadero ya no evaluo el siguiente (), 
//		        	si ninguno de las dos primeras es correcta hago el segundo, si es true , bisiesto 
//		        	y si no lo es se pasa al else
		        	
		        	System.out.println("El año " + a + " es bisiesto");
		        } 
		        
		        else {
		            System.out.println("El año " + a + " no es bisiesto");
		        }
		 
	}
}
	
