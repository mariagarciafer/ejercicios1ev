package unidad3;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class BisiestoConCalendar {
	

	 

	 
	    public static void main(String[] args) {
	 
	        Scanner sn =new Scanner(System.in);
	 
	        // Pido el año
	        System.out.println("Introduce un año");
	        int anio = sn.nextInt();
	 
	        // Creo un calendario gregoriano
	        GregorianCalendar calendar = new GregorianCalendar();
	 
	        // isLeapYear indica si el año es o no bisiesto
	        if (calendar.isLeapYear(anio)) {
	            System.out.println("El año " + anio + " es bisiesto");
	        } else {
	            System.out.println("El año " + anio + " no es bisiesto");
	        }
	 
	    }
}
	 

