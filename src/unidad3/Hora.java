package unidad3;

import java.util.Scanner;

public class Hora {

	public static void main(String[] args) {
//		ifs anidados, ojo con el prefijo y el postfijo de s,m ++, también se puede poner como if (++s==60)
		
//		1º variables para recoger los datos de entrada
			int h;
			int m;
			int s;
			
//			2º Clase Scanner par aller los datos de teclado
			Scanner in = new Scanner(System.in);
			
//			3º uso las mismas variables para almacenar los datos de salida e interacciono con usuario
			System.out.println("Hora: ");
			h = in.nextInt();
			System.out.println("Minuto: ");
			m = in.nextInt();
			System.out.println("Segundo: ");
			s = in.nextInt();
			
//			4º incremento los segundos en una unidad ..
			s++;
//			puedo ahorrarme está linea si pongo if(++s ==60); 
//			primero incremento y luego evaluo, si pongo s++ sería al reves, evaluo y luego incremento.
			
//			5º Devolver el resultado
			if (s == 60) {
				s = 0;
				m++;
				if (m == 60) {
					m = 0;
					h++;
					if (h == 23)
						h = 0;
				}
			}
			
			System.out.println("La hora correspondiente al minuto siguiente es: " + h + ":" + m + ":" + s);
		}
}
// Otra forma de hacerlo 
//		public static void main(String[] args) {
//			
//		Scanner in = new Scanner(System.in);
//			
//		System.out.print("Hora: ");
//		int hora = in.nextInt();
//		System.out.print("Minutos: ");
//		int minutos = in.nextInt();
//		System.out.print("Segundos: ");
//		int segundos = in.nextInt();
//		segundos++;
//	
//			if (segundos > 59) {
//				segundos = 0;
//				minutos++;
//				
//				if (minutos > 59) {
//					minutos = 0;
//					hora++;
//					
//					if (hora > 23)
//						hora = 0;
//		}
//	}
//			System.out.println(hora + ":" + minutos + ":" + segundos);
//}

	
