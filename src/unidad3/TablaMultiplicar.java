package unidad3;

import java.util.Scanner;

public class TablaMultiplicar {

	public static void main(String[] args) {
//	No sabemos cuantas veces se va a repetir el proceso 
//		ya que depende del cuantas veces quiera repasar el usuario, 
//		por tanto bucle ...while o do while.
//	Cual de ellos depende de la interfaz, si le quiero preguntar al usuario 
//		si quiere repasar alguno o directamente le propongo una.
	
		
		Scanner in = new Scanner(System.in);
		String respuesta;
		int tabla; 
		int resultado; 
		int fallos; 
		do {
			System.out.println("¿Que tabla deseas repasar? Introduce el número: ");
//			Incicalizo la variable fallos a 0 antes del comienzo del bucle for
			fallos = 0;
//			tabla , la variable para leer el número que queremos repasar
			tabla = Integer.parseInt(in.nextLine());
			
//			para repasar la tabla que toque necesito un bucle for, conozco el número de iteracciones ( 
			
			for (int i=1; i <=9; i++) {
				System.out.println("¿" + tabla + "x" + i + "?");
				resultado = Integer.parseInt(in.nextLine());
					if (resultado == (tabla*i)) {
						System.out.println("Correcto!!");
						}
					else {
//						incrementamos entonces el número de fallos
						fallos++; 
						System.out.println("Incorrecto, el resultado es " + (tabla*i));
					}
			}
//		Fuera ya del bucle for: 
			
		if (fallos<2) {
			System.out.println("Enhorabuena, has aprobado!");
		}
		else {
			System.out.println("Lo siento, has suspendido");
		}
		System.out.println("¿Desea repasar otra tabla de multiplicar?(s/n): ");
		respuesta = in.nextLine();
		} while (respuesta.equalsIgnoreCase("s")); {
			
			
	}

}

	}




