package unidad2;


	import java.io.BufferedReader;
	import java.io.IOException;
	import java.io.InputStreamReader;


		public class Conversor {
//		puedo declarar las variables aquí tambien, dentro de la clase pero fuera del método,
//			en este caso si tienen valos asignado, el que venga por defecto en su tipo de variable. 
//			Añado el identificador static en este caso.
//			static float euros; 
//			static float dolares;
//			static String linea;
//			
			public static void main(String[] args) throws IOException {
//				declaro las variables, indeterminadas, no tienen valor definido. 
//				Se crean cuando se invoca el método
//				float euros; 
//				float dolares;
//				String linea;
//				
//		Necesito entrada de datos por teclado (los euros a convertir) --BufferedReader--
//				y necesito que mi programa lea una linea de texto que el usuario introduce 
//				por teclado --System.out...para sacar la linea y in.Readline para leerla.
//				Con parsefloat realizo conversion (forzada=explicita) de una linea de texto a un n�mero.
				
//				BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
//				System.out.println("Cuantos euros quieres convertir?");
//				linea = in.readLine();
//				euros = Float.parseFloat(linea);
//				dolares = euros * 1.17f;
//				System.out.printf("%-15.2f", dolares);
//				
//			}
//		}

//				Más abreviado: 
				
				BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
				System.out.println("Cuantos euros quieres convertir?");	
				System.out.printf("%-15.2f",Float.parseFloat(in.readLine())* 1.17f);
				
			}
		}
		


