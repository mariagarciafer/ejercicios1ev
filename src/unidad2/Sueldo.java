package unidad2;

import java.util.Scanner;

public class Sueldo {


//		En el método main de una clase Java llamada Sueldo escribe un programa que
//		resuelva el problema siguiente: un vendedor recibe un sueldo base más un 10%
//		extra por comisión de sus ventas; el vendedor desea saber cuánto dinero
//		obtendrá por concepto de comisiones, por las tres ventas que realiza en el mes
//		y el total que recibirá en el mes, teniendo en cuenta su sueldo base y las
//		comisiones. Se utilizará el teclado para la entrada de datos y la consola para la
//		salida. Para resolver este problema está permitido utilizar la clase Scanner.
		
	public static void main(String[] args) {
			Scanner in = new Scanner(System.in);
			System.out.print("Sueldo base: ");
			float sueldo = in.nextFloat();
			System.out.print("Importe de la primera venta: ");
			float venta1 = in.nextFloat();
			System.out.print("Importe de la segunda venta: ");
			float venta2 = in.nextFloat();
			System.out.print("Importe de la tercera venta: ");
			float venta3 = in.nextFloat();
			float comision = (venta1 + venta2 + venta3) * 0.1f;
			System.out.printf("Comisión por ventas: %.2f\nSueldo total: %.2f\n", comision, sueldo + comision);
		}

	}


