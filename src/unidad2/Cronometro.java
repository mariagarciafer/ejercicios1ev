 package unidad2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Cronometro {

	public static void main(String[] args) throws IOException {
		
//		En el método main de una clase Java llamada Cronometro escribe un programa
//		que pida al usuario que introduzca mediante el teclado su nombre y a
//		continuación muestre en la consola un mensaje que incluya un número real que
//		represente los segundos y milisegundos que ha tardado en contestar, con un
//		formato de salida que emplee exactamente 3 dígitos en la parte decimal.
//		Realiza el ejercicio sin utilizar la clase Scanner.
		
//		

			String nombre;
			BufferedReader in= new BufferedReader(new InputStreamReader(System.in));
			long t0 = System.currentTimeMillis();
			System.out.println("Teclea tu nombre: ");
			nombre = in.readLine();
			long t1 = System.currentTimeMillis() - t0;
			long segundos = t1 / 1000;
			long milis = t1 % 1000;
//			tantos args como especificadores de formato, 3 %, 3 args : nombre, segundos , milis
			System.out.printf("Hola %s, has tardado %d,%3d segundos en decirme tu nombre", nombre, segundos, milis);
		}
}

