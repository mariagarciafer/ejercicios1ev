//package unidad4;
//
//import java.util.Scanner;
//
//public class VectorPersonas {
//
//	public static void main(String[] args) {
//		Scanner in = new Scanner(System.in);
//		//		1º Declaro el array de objetos tipo persona.
//
//
//		Persona[] personas;
//
//		System.out.println("¿Cuantas personas quieres incluir?");
//		int numPersonas= in.nextInt();
//		//		2º Dimensiono el vector en tiempo de ejecución con ese número de personas
//
//		personas = new Persona[numPersonas];
//
//		//		3º declaro las variables para obtener los datos de cada persona: 
//
//		String nombre, sexo = null, cadena;
//		int edad; 
//
//		//		4º Recorrido y llenado del array: 
//
//		for (int i=0; i<personas.length; i++) {
//			System.out.println("Introduce el nombre de la persona " + i+ ".");
//			nombre= in.nextLine();
//			System.out.println("Introduce la edad de la persona " + i + ".");
//			edad= in.nextInt();
//			//			5º Usamos una variable lógica para asegurar que el valor leido para sexo es correcto.
//			boolean correcto=false;
//			//			6º y ahora se repite la lectrura de sexo hasta que se introduzca un valor correcto--> bucle do while.
//
//			do {
//				System.out.println("Introduce el sexo de la persona" + i + ":\n H: hombre, M: mujer, D: desconocido. ");
//				cadena=in.nextLine();
//				if(cadena.length()!=1) {
//					System.out.println("El sexo se indica con una sola letra\n H: hombre, M: mujer, D: desconocido.");
//				}else {
//					switch(cadena.charAt(0)) {
//					case 'H': case'M':case 'D': case'm':case 'h': case'd':
//						correcto=true;
//						break;
//					default:
//						correcto=false;
//						System.out.println(" Letra no válida");
//					}
//				}
//			}while (cadena.length()!=1 || !correcto);
//
//			cadena=cadena.toUpperCase();
//			//			sexo=cadena.charAt(0);
//
//
//// 7º Inserto las personas en el array, ordenadas por edad y sin dejar huecos libres.
//			int j=0;
//			
//			Persona nuevaPersona= new Persona();
//			nuevaPersona.ponNombre(nombre);
//			nuevaPersona.ponEdad(edad);
//			nuevaPersona.ponSexo(sexo);
////			Empezamos a recorrer el vector hasta que encontramos un hueco
//			while(personas[j]!=null && personas[j].obtenerEdad()<=nuevaPersona.obtenerEdad()){
//					j++;
//			}
//			if(personas[j]==null)
//				personas[j]=nuevaPersona;
//			else {
//				for(int k=personas.length - 1; k>j; k--) {
//					personas[k]=personas[k-1];
//				}
//				personas[j]=nuevaPersona;
//			}
//			System.out.println("\n");
//		}
//// for cerrado
//
////		8º Procesar el array
//		for (int i=0; i<personas.length && personas[i]!=null;i++) {
//			System.out.println(i+ ": "+ personas[i]);
//		}
//		double sumaEdades=0, edadMedia=0;
//		for(int i=0; i<personas.length; i++) {
//			sumaEdades+=personas[i].obtenerEdad();
//		}
//		edadMedia=sumaEdades/personas.length;
//		System.out.println("la edad media es de: "+ edadMedia);
//
//		
//		
//		
//		
//		
//	}
//
//}
//
//

