package unidad4;

public class Persona {

	// Concurso de postales navideñas de una empresa, se agrupa a los participantes por edad, sexo...
	
//		1º Que define a cada persona
	
		private String nombre; 
		private int edad; 
		private String sexo; 
		
//		2º Constructor para inicializar cada nueva persona

		public Persona(String nombre, int edad, String sexo) {
			
			this.nombre=nombre;
			
			if(analizaEdad(edad))
				this.edad=edad;
			else {
				System.out.println("Edad no válida, no se ha modificado la edad de la persona.");
				this.edad=0;
			}
			if(analizaSexo(sexo)) 
				this.sexo=sexo;
			else {
					System.out.println("No válido, no se ha modificado el sexo de la persona.");
					this.sexo="D";
				}
			}


	

		//		4º Métodos públicos de acceso a los campos privados de la clase: (getters y setters)
		public void ponNombre(String nombre) {
			this.nombre=nombre;
		}
		public void ponEdad(int edad) {
			if(analizaEdad(edad))
				this.edad=edad;
			else {
				System.out.println("Edad no válida, no se ha modificado la edad de la persona.");
				}
		}
		public void ponSexo(String sexo) {
			if(analizaSexo(sexo)) 
				this.sexo=sexo;
			else {
					System.out.println("No válido, no se ha modificado el sexo de la persona.");
				}
		}
		
		public String obtenerNombre() {
			return nombre;
		}
		public int obtenerEdad() {
			return edad;
		}
		public String obtenerSexo() {
			return sexo;
		}

//	3º Defino los métodos para comprobar que los datos introducidos son correctos: 
		
		public boolean analizaEdad(int edad) {
			boolean correcta = true;
			if (edad<0) {
				correcta=false;
			}
		
			return correcta; 
		}
			
		public boolean analizaSexo(String sexo) {
			boolean correcto = true;
			if (sexo != "D" && sexo !="M" && sexo !="H") {
				correcto=false;
			}
			return correcto;
		}
	
		
	}
//Fin, ahora paso a la clase VectorPersonas para empezar a rellenar.


