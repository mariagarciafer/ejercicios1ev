package unidad4;

import java.util.Random;
import java.util.Scanner;

public class Ejercicio7 {

	public static void main(String[] args) {
		Random r = new Random();
		Scanner in = new Scanner(System.in);
		// 1º declaro la variable
		int[] vector;
		System.out.println("Introduce la dimensión del vector (10<= dimensión <=1.000.000");
//		2º leer de teclado....
		int dim = in.nextInt();
		vector = new int[dim];
		long t0 = System.currentTimeMillis();
//		3º Creo el vector con esa dimensión y lo lleno con números aleatorios usando el bucle for.
		for (int i=0; i<vector.length; i++) {
//			5º generar un número aleatorio en cada paso del bucle, y hago una busqueda en el vector, si ya está, no lo almaceno, al contrario si
			int n;
			int j;
			do {
				 n = r.nextInt(2000000) - 999999;
	//			 6º Hago una búsqueda secuencial con un bucle while
				 
				  j=0;
				  
				 while (j < i && vector[j] != n)
					 
					 j++;
			} while(j != i);
//			7º Relleno la matriz
			 vector[i] = n;	
//			4º
//			vector[i] = r.nextInt(valormax-valormin+1) +
//			( ojo esta suma se transforma en resta porque es un valor negativo)  valor min
//formula para el método nextInt, para métodos aleatorios comprendidos en un rango
//			vector[i] =  r.nextInt(2000000) - 999999;
			 
			 long t1 = System.currentTimeMillis() - t0;
				System.out.println("Tamaño del vector: " + vector.length);
				System.out.println("Tiempo empleado en llenarlo: " + t1 + " milisegundos");
				t0 = System.currentTimeMillis();
				int dif = 0;
				
		 		t1 = System.currentTimeMillis() - t0;
		 		System.out.println("Diferencia entre el valor mayor y el menor: " + dif);
				System.out.println("Tiempo empleado en calcular la diferencia: " + t1 + "milisegundos");
			}

		}
	}

